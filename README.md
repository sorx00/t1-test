[![pipeline status](https://gitlab.com/sorx00/t1-test/badges/master/pipeline.svg)](https://gitlab.com/sorx00/t1-test/commits/master)

Телематика-Один
===============

Тестовое задание на позицию DevOps Engineer
-------------------------------------------

CI/CD конвейер для простого приложения (веб
сервер, отображающий html страницу), используя GitLabCI.
